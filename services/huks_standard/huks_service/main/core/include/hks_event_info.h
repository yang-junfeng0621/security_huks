/*
 * Copyright (c) 2025-2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HKS_EVENT_INFO_H
#define HKS_EVENT_INFO_H

#include <stdbool.h>
#include <stdint.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

#define MAP_NAME_SIZE 32

typedef struct HksEventKeyAccessInfo {
    uint32_t authType;
    uint32_t accessType;
    uint32_t challengeType;
    uint32_t challengePos;
    uint32_t authTimeOut;
    uint32_t authPurpose;
    uint32_t frontUserId;
    uint32_t authMode;
    uint32_t needPwdSet;
} HksEventKeyAccessInfo;

typedef struct HksEventKeyInfo {
    uint8_t aliasHash;
    uint32_t storageLevel;
    int32_t specificUserId;
    uint32_t alg;
    uint32_t purpose;
    uint32_t keySize;
    uint32_t keyFlag;
    uint16_t keyHash;
    bool isBatch;
    uint32_t batchPur;
    uint32_t batchTimeOut;
} HksEventKeyInfo;

typedef struct HksEventStatInfo {
    uint32_t saCost;
    uint32_t ca2taCost;
    uint32_t taCost;
    uint32_t gpCost;
    uint32_t initCost;
    uint32_t updateCost;
    uint32_t finishCost;
    uint32_t totalCost;
} HksEventStatInfo;

typedef struct HksEventCryptoInfo {
    HksEventKeyInfo keyInfo;
    HksEventKeyAccessInfo accessCtlInfo;
    HksEventStatInfo stat;
    uint32_t blockMode;
    uint32_t padding;
    uint32_t digest;
    uint32_t mgfDigest;
    uint32_t dataLen;
    uint32_t handleId;
} HksEventCryptoInfo;

typedef struct HksEventAgreeDeriveInfo {
    HksEventKeyInfo keyInfo;
    HksEventKeyAccessInfo accessCtlInfo;
    HksEventStatInfo stat;
    uint32_t iterCnt;
    uint32_t storageFlag;
    uint32_t keySize;
    uint32_t pubKeyType;
    uint32_t aliasHash;
    uint32_t handleId;
} HksEventAgreeDeriveInfo;

typedef struct HksEventMacInfo {
    HksEventKeyInfo keyInfo;
    HksEventKeyAccessInfo accessCtlInfo;
    HksEventStatInfo stat;
    uint32_t dataLen;
    uint32_t handleId;
} HksEventMacInfo;

typedef struct HksEventAttestInfo {
    HksEventKeyInfo keyInfo;
    HksEventStatInfo stat;
    uint32_t baseCertType;
    bool isAnnonymous;
} HksEventAttestInfo;

typedef struct HksEventResultInfo {
    uint32_t stage;
    uint32_t module;
    uint32_t code;
    const char *errMsg;
} HksEventResultInfo;

typedef struct HksEventCallerInfo {
    uint32_t uid;
    char *name;
} HksEventCallerInfo;

typedef struct HksEventCommonInfo {
    uint32_t eventId;
    char *function;
    uint32_t operation;
    uint32_t count;
    struct timespec time;
    HksEventCallerInfo callerInfo;
    HksEventResultInfo result;
} HksEventCommonInfo;

typedef struct GenerateInfo {
    struct HksEventKeyInfo keyInfo;
    struct HksEventKeyAccessInfo keyAccessInfo;
    uint32_t agreeAlg;
    uint32_t pubKeyIsAlias;
    struct HksEventStatInfo statInfo;
} GenerateInfo;

typedef struct ImportInfo {
    struct HksEventKeyInfo keyInfo;
    struct HksEventKeyAccessInfo keyAccessInfo;
    uint32_t keyType;
    uint32_t algSuit;
    struct HksEventStatInfo statInfo;
} ImportInfo;

typedef struct RenameInfo {
    struct HksEventKeyInfo keyInfo;
    uint8_t dstAliasHash;
    bool isCopy;
    struct HksEventStatInfo statInfo;
} RenameInfo;

typedef struct HksEventInfo {
    struct HksEventCommonInfo common;
    union {
        HksEventCryptoInfo cryptoInfo;
        HksEventAgreeDeriveInfo agreeDeriveInfo;
        HksEventMacInfo macInfo;
        HksEventAttestInfo attestInfo;
        HksEventKeyInfo keyInfo;
        HksEventKeyAccessInfo keyAccessInfo;
        HksEventStatInfo statInfo;
        GenerateInfo generateInfo;
        ImportInfo importInfo;
        RenameInfo renameInfo;
    };
} HksEventInfo;

typedef struct HksEventItemMap {
    char name[MAP_NAME_SIZE];
    uint32_t value;
} HksEventItemMap;

#ifdef __cplusplus
}
#endif

#endif  // HKS_EVENT_INFO_H
