/*
 * Copyright (c) 2025-2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HKS_EVENT_PROC_H
#define HKS_EVENT_PROC_H

#include <stdint.h>

#include "hks_event_info.h"
#include "hks_report_three_stage.h"
#include "hks_type.h"
#include "hks_type_enum.h"

#ifdef __cplusplus
extern "C" {
#endif

const static HksEventProcMap HKS_EVENT_HANDLERS[] = {
    { HKS_EVENT_CRYPTO, HksParamSetToEventInfoCrypto, HksEventInfoNeedReportCrypto, 
        HksEventInfoIsEqualCrypto, HksEventInfoAddCrypto, HksEventInfoToMapCrypto },
    { HKS_EVENT_AGREE_DERIVE, HksParamSetToEventInfoAgreeDerive, HksEventInfoNeedReportAgreeDerive,
        HksEventInfoIsEqualAgreeDerive, HksEventInfoAddAgreeDerive, HksEventInfoToMapAgreeDerive },
    { HKS_EVENT_MAC, HksParamSetToEventInfoMac, HksEventInfoNeedReportMac, HksEventInfoIsEqualMac,
        HksEventInfoAddMac, HksEventInfoToMapMac },
    { HKS_EVENT_ATTEST, HksParamSetToEventInfoAttest, HksEventInfoNeedReportAttest, HksEventInfoIsEqualAttest,
        HksEventInfoAddAttest, HksEventInfoToMapAttest },
};

#ifdef __cplusplus
}
#endif

#endif  // HKS_EVENT_PROC_H
