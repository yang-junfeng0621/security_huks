/*
 * Copyright (c) 2025-2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HKS_REPORT_THREE_STAGE_H
#define HKS_REPORT_THREE_STAGE_H

#include <stdint.h>

#include "hks_event_info.h"
#include "hks_type.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef int32_t (*HksParamSetToEventInfo)(const struct HksParamSet *paramSet, HksEventInfo *keyInfo);

typedef bool (*HksEventInfoNeedReport)(const HksEventInfo *eventInfo);

typedef bool (*HksEventInfoIsEqual)(const HksEventInfo *info1, const HksEventInfo *info2);

typedef void (*HksEventInfoAdd)(HksEventInfo *info, const HksEventInfo *entry);

typedef int32_t (*HksEventInfoToMap)(const HksEventInfo *info, HksEventItemMap *map, uint32_t *num);

typedef struct HksEventProcMap {
    uint32_t eventId;
    HksParamSetToEventInfo eventInfoCreate;
    HksEventInfoNeedReport needReport;
    HksEventInfoIsEqual eventInfoEqual;
    HksEventInfoAdd eventInfoAdd;
    HksEventInfoToMap eventInfoToMap;
} HksEventProcMap;

int32_t HksParamSetToEventInfoCrypto(const struct HksParamSet *paramSet, HksEventInfo *eventInfo);

bool HksEventInfoNeedReportCrypto(const HksEventInfo *eventInfo);

bool HksEventInfoIsEqualCrypto(const HksEventInfo *info1, const HksEventInfo *info2);

void HksEventInfoAddCrypto(HksEventInfo *info, const HksEventInfo *entry);

int32_t HksEventInfoToMapCrypto(const HksEventInfo *info, HksEventItemMap *map, uint32_t *num);

int32_t HksParamSetToEventInfoAgreeDerive(const struct HksParamSet *paramSet, HksEventInfo *eventInfo);

bool HksEventInfoNeedReportAgreeDerive(const HksEventInfo *eventInfo);

bool HksEventInfoIsEqualAgreeDerive(const HksEventInfo *info1, const HksEventInfo *info2);

void HksEventInfoAddAgreeDerive(HksEventInfo *info, const HksEventInfo *entry);

int32_t HksEventInfoToMapAgreeDerive(const HksEventInfo *info, HksEventItemMap *map, uint32_t *num);

int32_t HksParamSetToEventInfoMac(const struct HksParamSet *paramSet, HksEventInfo *eventInfo);

bool HksEventInfoNeedReportMac(const HksEventInfo *eventInfo);

bool HksEventInfoIsEqualMac(const HksEventInfo *info1, const HksEventInfo *info2);

void HksEventInfoAddMac(HksEventInfo *info, const HksEventInfo *entry);

int32_t HksEventInfoToMapMac(const HksEventInfo *info, HksEventItemMap *map, uint32_t *num);

int32_t HksParamSetToEventInfoAttest(const struct HksParamSet *paramSet, HksEventInfo *eventInfo);

bool HksEventInfoNeedReportAttest(const HksEventInfo *eventInfo);

bool HksEventInfoIsEqualAttest(const HksEventInfo *info1, const HksEventInfo *info2);

void HksEventInfoAddAttest(HksEventInfo *info, const HksEventInfo *entry);

int32_t HksEventInfoToMapAttest(const HksEventInfo *info, HksEventItemMap *map, uint32_t *num);

#ifdef __cplusplus
}
#endif

#endif  // HKS_REPORT_THREE_STAGE_H
