/*
 * Copyright (c) 2025-2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HKS_REPORT_COMMON_BUILD_H
#define HKS_REPORT_COMMON_BUILD_H

#include <stdint.h>

#include "hks_event_info.h"
#include "hks_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int32_t GetReportInfo(const struct HksParamSet *paramSet, HksOperationReport **report);

void BuildCommonInfo(const struct HksParamSet *paramSet, struct HksEventCommonInfo *commonInfo,
    const HksOperationReport *report);

int32_t BuildKeyInfo(const struct HksParamSet *paramSet, struct HksEventKeyInfo *keyInfo,
    const HksOperationReport *report);

void BuildKeyAccessInfo(const struct HksParamSet *paramSet, struct HksEventKeyAccessInfo *accessInfo,
    const HksOperationReport *report);

void BuildCryptoInfo(const struct HksParamSet *paramSet, struct HksEventCryptoInfo *cryptoInfo,
    const HksOperationReport *report);

void BuildAgreeDeriveInfo(const struct HksParamSet *paramSet, struct HksEventAgreeDeriveInfo *agreeDeriveInfo,
    const HksOperationReport *report);

void BuildMacInfo(const struct HksParamSet *paramSet, struct HksEventMacInfo *macInfo,
    const HksOperationReport *report);

// check uid, eventId, operation, specificUserId, aliasHash, keyHash
bool CheckEventCommon(const struct HksEventInfo *info1, const struct HksEventInfo *info2);

// add count, dataLen, totalCost
void AddEventInfoCommon(HksEventInfo *info, const HksEventInfo *entry);

void KeyInfoToMap(const HksEventKeyInfo *keyInfo, HksEventItemMap *map, uint32_t *num);

void KeyAccessInfoToMap(const HksEventKeyAccessInfo *accessInfo, HksEventItemMap *map, uint32_t *num);

void CryptoInfoToMap(const HksEventCryptoInfo *cryptoInfo, HksEventItemMap *map, uint32_t *num);

void AgreeDeriveInfoToMap(const HksEventAgreeDeriveInfo *agreeDeriveInfo, HksEventItemMap *map, uint32_t *num);

void MacInfoToMap(const HksEventMacInfo *macInfo, HksEventItemMap *map, uint32_t *num);


#ifdef __cplusplus
}
#endif

#endif  // HKS_REPORT_COMMON_BUILD_H
