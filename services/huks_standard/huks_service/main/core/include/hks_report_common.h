/*
 * Copyright (c) 2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HKS_REPORT_COMMON_H
#define HKS_REPORT_COMMON_H

#include "hks_template.h"
#include "hks_type.h"
#include "hks_type_inner.h"
#include "hks_event_info.h"
#include "hks_type_enum.h"
#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif

#define HASH_SHA256_SIZE 256
#define KEYALIAS_HASH_SHA256_SIZE 1
#define KEY_HASH_SHA256_SIZE 2

int32_t AddTimeCost(struct HksParamSet *paramSetOut, uint64_t startTime);

int32_t AddKeyHash(struct HksParamSet *paramSetOut, const struct HksBlob *keyIn);

int32_t AddKeyAliasHash(struct HksParamSet *paramSetOut, const struct HksBlob *keyAlias);

int32_t AddCommonInfo(const char *funcName, const struct HksProcessInfo *processInfo,
    int32_t errorCode, struct HksParamSet *reportParamSet);

int32_t GetCommonEventInfo(const struct HksParamSet *paramSetIn, struct HksEventInfo *eventInfo);

int32_t GetEventKeyInfo(const struct HksParamSet *paramSetIn, struct HksEventKeyInfo *keyInfo);

int32_t GetEventKeyAccessInfo(const struct HksParamSet *paramSetIn, struct HksEventKeyAccessInfo *keyAccessInfo);

void EventInfoToMapKeyInfo(const struct HksEventInfo *eventInfo, struct HksEventItemMap *map, uint32_t *num);

void EventInfoToMapKeyAccessInfo(const struct HksEventInfo *eventInfo, struct HksEventItemMap *map, uint32_t *num);

#define HKS_FREE_REPORT_PARAMSET(preParamSet, paramSet) \
do { \
    HksFreeParamSet(&preParamSet); \
    HksFreeParamSet(&paramSet); \
} while (0);

#ifdef __cplusplus
}
#endif

#endif  // HKS_REPORT_COMMON_H
