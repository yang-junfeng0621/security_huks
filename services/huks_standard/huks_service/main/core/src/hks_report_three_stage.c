/*
 * Copyright (c) 2025-2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hks_event_proc.h"

#include <stdint.h>
#include <string.h>
#include <sys/stat.h>

#include "hks_event_info.h"
#include "hks_log.h"
#include "hks_report_common_build.h"
#include "hks_template.h"
#include "hks_type.h"
#include "hks_type_enum.h"
#include "stdbool.h"

int32_t HksParamSetToEventInfoCrypto(const struct HksParamSet *paramSet, HksEventInfo *eventInfo)
{
    HksOperationReport *report = NULL;
    int32_t ret = GetReportInfo(paramSet, &report);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "get report info fail")
    BuildCommonInfo(paramSet, &eventInfo->common, report);
    (void)BuildKeyInfo(paramSet, &eventInfo->cryptoInfo.keyInfo, report);
    BuildKeyAccessInfo(paramSet, &eventInfo->cryptoInfo.accessCtlInfo, report);
    BuildCryptoInfo(paramSet, &eventInfo->cryptoInfo, report);
    return HKS_SUCCESS;
}

bool HksEventInfoNeedReportCrypto(const HksEventInfo *eventInfo)
{
    return eventInfo->common.result.code != HKS_SUCCESS;
}

bool HksEventInfoIsEqualCrypto(const HksEventInfo *info1, const HksEventInfo *info2)
{
    return CheckEventCommon(info1, info2);
}

void HksEventInfoAddCrypto(HksEventInfo *info, const HksEventInfo *entry)
{
    AddEventInfoCommon(info, entry);
}

int32_t HksEventInfoToMapCrypto(const HksEventInfo *info, HksEventItemMap *map, uint32_t *num)
{
    KeyInfoToMap(&(info->cryptoInfo.keyInfo), map, num);
    KeyAccessInfoToMap(&(info->cryptoInfo.accessCtlInfo), map, num);
    CryptoInfoToMap(&(info->cryptoInfo), map, num);
    return HKS_SUCCESS;
}

int32_t HksParamSetToEventInfoAgreeDerive(const struct HksParamSet *paramSet, HksEventInfo *eventInfo)
{
    HksOperationReport *report = NULL;
    int32_t ret = GetReportInfo(paramSet, &report);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "get report info fail")
    BuildCommonInfo(paramSet, &eventInfo->common, report);
    (void)BuildKeyInfo(paramSet, &eventInfo->agreeDeriveInfo.keyInfo, report);
    BuildKeyAccessInfo(paramSet, &eventInfo->agreeDeriveInfo.accessCtlInfo, report);
    BuildAgreeDeriveInfo(paramSet, &eventInfo->agreeDeriveInfo, report);
    return HKS_SUCCESS;
}

bool HksEventInfoNeedReportAgreeDerive(const HksEventInfo *eventInfo)
{
    return eventInfo->common.result.code != HKS_SUCCESS;
}

bool HksEventInfoIsEqualAgreeDerive(const HksEventInfo *info1, const HksEventInfo *info2)
{
    return CheckEventCommon(info1, info2);
}

void HksEventInfoAddAgreeDerive(HksEventInfo *info, const HksEventInfo *entry)
{
    AddEventInfoCommon(info, entry);
}

int32_t HksEventInfoToMapAgreeDerive(const HksEventInfo *info, HksEventItemMap *map, uint32_t *num)
{
    KeyInfoToMap(&(info->agreeDeriveInfo.keyInfo), map, num);
    KeyAccessInfoToMap(&(info->agreeDeriveInfo.accessCtlInfo), map, num);
    AgreeDeriveInfoToMap(&(info->agreeDeriveInfo), map, num);
    return HKS_SUCCESS;
}

int32_t HksParamSetToEventInfoMac(const struct HksParamSet *paramSet, HksEventInfo *eventInfo)
{
    HksOperationReport *report = NULL;
    int32_t ret = GetReportInfo(paramSet, &report);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "get report info fail")
    BuildCommonInfo(paramSet, &eventInfo->common, report);
    (void)BuildKeyInfo(paramSet, &eventInfo->macInfo.keyInfo, report);
    BuildKeyAccessInfo(paramSet, &eventInfo->macInfo.accessCtlInfo, report);
    BuildMacInfo(paramSet, &eventInfo->macInfo, report);
    return HKS_SUCCESS;
}

bool HksEventInfoNeedReportMac(const HksEventInfo *eventInfo)
{
    return eventInfo->common.result.code != HKS_SUCCESS;
}

bool HksEventInfoIsEqualMac(const HksEventInfo *info1, const HksEventInfo *info2)
{
    return CheckEventCommon(info1, info2);
}

void HksEventInfoAddMac(HksEventInfo *info, const HksEventInfo *entry)
{
    AddEventInfoCommon(info, entry);
}

int32_t HksEventInfoToMapMac(const HksEventInfo *info, HksEventItemMap *map, uint32_t *num)
{
    KeyInfoToMap(&(info->macInfo.keyInfo), map, num);
    KeyAccessInfoToMap(&(info->macInfo.accessCtlInfo), map, num);
    MacInfoToMap(&(info->macInfo), map, num);
    return HKS_SUCCESS;
}

int32_t HksParamSetToEventInfoAttest(const struct HksParamSet *paramSet, HksEventInfo *eventInfo)
{
    return HKS_SUCCESS;
}

bool HksEventInfoNeedReportAttest(const HksEventInfo *eventInfo)
{
    return eventInfo->common.result.code != HKS_SUCCESS;
}

bool HksEventInfoIsEqualAttest(const HksEventInfo *info1, const HksEventInfo *info2)
{
    return CheckEventCommon(info1, info2);
}

void HksEventInfoAddAttest(HksEventInfo *info, const HksEventInfo *entry)
{
    AddEventInfoCommon(info, entry);
}

int32_t HksEventInfoToMapAttest(const HksEventInfo *info, HksEventItemMap *map, uint32_t *num)
{
    return HKS_SUCCESS;
}
