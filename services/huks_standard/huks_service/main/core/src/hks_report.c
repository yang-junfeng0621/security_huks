/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hks_report.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "hks_api.h"
#include "hks_log.h"
#include "hks_param.h"
#include "hks_template.h"
#include "hks_type.h"

#ifdef HKS_CONFIG_FILE
#include HKS_CONFIG_FILE
#else
#include "hks_config.h"
#endif

#ifdef L2_STANDARD
#include "hks_report_wrapper.h"
#endif

#define HASH_SHA256_SIZE 32

static int32_t GetHash(const struct HksBlob *data, struct HksBlob *hash)
{
    struct HksParam hashParams[] = {
        { .tag = HKS_TAG_DIGEST, .uint32Param = HKS_DIGEST_SHA256 },
    };

    struct HksParamSet *hashParamSet = NULL;
    int32_t ret = HksInitParamSet(&hashParamSet);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "init paramset fail");

    do {
        ret = HksAddParams(hashParamSet, hashParams, HKS_ARRAY_SIZE(hashParams));
        HKS_IF_NOT_SUCC_LOGE_BREAK(ret, "add params fail");

        ret = HksHash(hashParamSet, data, hash);
        HKS_IF_NOT_SUCC_LOGE_BREAK(ret, "hash fail");
    } while (0);

    HksFreeParamSet(&hashParamSet);
    return ret;
}

int32_t GetKeyAliasHash(const struct HksBlob *keyAlias, uint8_t *keyAliasHash)
{
    uint8_t hashData[HASH_SHA256_SIZE] = { 0 };
    struct HksBlob hash = { HASH_SHA256_SIZE, hashData };
    int32_t ret = GetHash(keyAlias, &hash);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "get keyAlias hash fail");
    *keyAliasHash = hash.data[hash.size - 1];
    return HKS_SUCCESS;
}

int32_t GetKeyHash(const struct HksBlob *key, uint16_t *keyHash)
{
    uint8_t hashData[HASH_SHA256_SIZE] = { 0 };
    struct HksBlob hash = { HASH_SHA256_SIZE, hashData };
    int32_t ret = GetHash(key, &hash);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "get key hash fail");
    *keyHash = 0;
    *keyHash |= hash.data[hash.size - 2] << 8;
    *keyHash |= hash.data[hash.size - 1];

    return HKS_SUCCESS;
}

void HksEventReport(const char *funcName, const struct HksProcessInfo *processInfo,
    const struct HksParamSet *paramSetIn, const struct HksParamSet *reportParamSet, int32_t errorCode)
{

}

void HksReport(const char *funcName, const struct HksProcessInfo *processInfo,
    const struct HksParamSet *paramSetIn, int32_t errorCode)
{
#ifdef L2_STANDARD
    int32_t ret = ReportFaultEvent(funcName, processInfo, paramSetIn, errorCode);
    HKS_IF_NOT_SUCC_LOGE(ret, "report fault event failed, ret = %" LOG_PUBLIC "d", ret)
#else
    (void)funcName;
    (void)processInfo;
    (void)paramSetIn;
    (void)errorCode;
#endif
}
