/*
 * Copyright (c) 2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hks_base_check.h"
#include "hks_event_info.h"
#include "hks_mem.h"
#include "hks_param.h"
#include "hks_report.h"
#include "hks_report_import_key.h"
#include "hks_template.h"
#include "hks_type.h"
#include "hks_type_enum.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include "hks_storage_utils.h"
#include "hks_type_inner.h"
#include "securec.h"
#include "hks_api.h"
#include "hks_report_common.h"


int32_t PreConstructImportKeyReportParamSet(const struct HksBlob *keyAlias, const struct HksParamSet *paramSetIn,
    uint64_t startTime, const struct HksBlob *keyIn, struct HksParamSet *paramSetOut)
{
    int32_t ret = HksInitParamSet(&paramSetOut);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "ConstructGenKeyReportParamSet InitParamSet failed")

    ret = HksGetParamSet(paramSetIn, paramSetIn->paramsCnt, &paramSetOut);
    if (ret != HKS_SUCCESS) {
        HKS_LOG_E("fresh param set failed. ret = %" LOG_PUBLIC "d", ret);
        HksFreeParamSet(&paramSetOut);
    }

    ret = AddKeyHash(paramSetOut, keyIn);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "add key hash to params failed")

    ret = AddKeyAliasHash(paramSetOut, keyAlias);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "add KeyAlias hash to preReportParamSet failed");

    ret = AddTimeCost(paramSetOut, startTime);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "add time cost to preReportParamSet failed");

    ret = HksBuildParamSet(&paramSetOut);
    if (ret != HKS_SUCCESS) {
        HKS_LOG_E("build paramset failed");
        HksFreeParamSet(&paramSetOut);
    }
    return ret;
}

int32_t ConstructImportKeyReportParamSet(const char *funcName, const struct HksProcessInfo *processInfo,
    int32_t errorCode, const struct HksParamSet *preReportParamSet, struct HksParamSet *reportParamSet)
{
    int32_t ret = HksInitParamSet(&reportParamSet);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "ConstructGenKeyReportParamSet InitParamSet failed")
    ret = HksGetParamSet(preReportParamSet, preReportParamSet->paramsCnt, &reportParamSet);
    if (ret != HKS_SUCCESS) {
        HKS_LOG_E("fresh reportParamSet set failed. ret = %" LOG_PUBLIC "d", ret);
        HksFreeParamSet(&reportParamSet);
    }

    ret = AddCommonInfo(funcName, processInfo, errorCode, reportParamSet);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "add common info to reportParamSet failed")

    struct HksParam params[] = {
        {
            .tag = HKS_TAG_PARAM1_UINT32,
            .uint32Param = HKS_EVENT_IMPORT_KEY
        },
        {
            .tag = HKS_TAG_PARAM0_UINT32,
            .uint32Param = HKS_EVENT_IMPORT_KEY
        },

    };
    do {
        ret = HksAddParams(reportParamSet, params, sizeof(params) / sizeof(params[0]));
        HKS_IF_NOT_SUCC_LOGE_BREAK(ret, "add in params failed")
        ret = HksBuildParamSet(&reportParamSet);
        HKS_IF_NOT_SUCC_LOGE_BREAK(ret, "build paramset failed");
        return HKS_SUCCESS;
    }while (0);
    HksFreeParamSet(&reportParamSet);
    return ret;
}

int32_t HksParamSetToEventInfoForImport(const struct HksParamSet *paramSetIn, struct HksEventInfo *eventInfo)
{
    int32_t ret = GetCommonEventInfo(paramSetIn, eventInfo);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "report GetCommonEventInfo failed!  ret = %" LOG_PUBLIC "d", ret);

    ret = GetEventKeyInfo(paramSetIn, &(eventInfo->importInfo.keyInfo));
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "report GetEventKeyInfo failed!  ret = %" LOG_PUBLIC "d", ret);

    ret = GetEventKeyAccessInfo(paramSetIn, &(eventInfo->importInfo.keyAccessInfo));
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "report GetEventKeyAccessInfo failed!  ret = %" LOG_PUBLIC "d", ret);

    struct HksParam *paramToEventInfo;
    ret = HksGetParam(paramSetIn, HKS_TAG_IMPORT_KEY_TYPE, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "report get keyType failed! ret = %" LOG_PUBLIC "d", ret);
    eventInfo->importInfo.keyType = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_UNWRAP_ALGORITHM_SUITE, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "report get algSuit failed! ret = %" LOG_PUBLIC "d", ret);
    eventInfo->importInfo.algSuit = paramToEventInfo->boolParam;

    ret = HksGetParam(paramSetIn, HKS_TAG_PARAM3_UINT32, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "report get total time cost failed! ret = %" LOG_PUBLIC "d", ret);
    eventInfo->importInfo.statInfo.taCost = paramToEventInfo->uint32Param;
    return ret;
}

bool HksEventInfoIsNeedReportForImport(const struct HksEventInfo *eventInfo)
{
    if (eventInfo != NULL && eventInfo->common.result.code != 0) {
        return true;
    }
    return false;
}

bool HksEventInfoIsEqualForImport(const struct HksEventInfo *eventInfo1, const struct HksEventInfo *eventInfo2)
{
    if ((eventInfo1 != NULL) && (eventInfo2 != NULL) &&
        (eventInfo1->common.callerInfo.uid == eventInfo2->common.callerInfo.uid) &&
        (eventInfo1->common.eventId == eventInfo2->common.eventId) &&
        (eventInfo1->common.operation == eventInfo2->common.operation)
    ) {
        return true;
    }
    return false;
}

void HksEventInfoAddForImport(struct HksEventInfo *dstEventInfo, const struct HksEventInfo *srcEventInfo)
{
    if (HksEventInfoIsEqualForImport(dstEventInfo, srcEventInfo)) {
        dstEventInfo->common.count++;
    }
}


int32_t HksEventInfoToMapForImport(const struct HksEventInfo *eventInfo, struct HksEventItemMap *map, uint32_t *num)
{
    EventInfoToMapKeyInfo(eventInfo, map, num);

    EventInfoToMapKeyAccessInfo(eventInfo, map, num);

    struct HksEventItemMap *curMap = map + (*num)++;
    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "import_key_type");
    curMap->value = eventInfo->importInfo.keyType;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "unwrap_algorithm_suit");
    curMap->value = (uint32_t)eventInfo->importInfo.algSuit;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "total_cost");
    curMap->value = eventInfo->importInfo.statInfo.taCost;
    return 0;
}