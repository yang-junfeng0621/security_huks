/*
 * Copyright (c) 2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hks_report_common.h"
#include "hks_event_info.h"
#include "hks_mem.h"
#include "hks_param.h"
#include "hks_report.h"
#include "hks_report_generate_key.h"
#include "hks_template.h"
#include "hks_type.h"
#include "hks_type_enum.h"
#include "hks_storage_utils.h"
#include "hks_type_inner.h"
#include "securec.h"
#include "hks_api.h"
#include "hks_util.h"
#include <stdint.h>
#include <string.h>

/*
HKS_TAG_PARAM0_UINT32 -> eventId
HKS_TAG_PARAM0_BUFFER -> function
HKS_TAG_PARAM1_UINT32 -> operation
HKS_TAG_PARAM1_BUFFER -> time
HKS_TAG_PARAM2_BUFFER -> processName
HKS_TAG_PARAM2_UINT32 -> processUid
HKS_TAG_PARAM3_BUFFER -> result
HKS_TAG_PARAM3_UINT32 -> timeCost
HKS_TAG_PARAM0_NULL -> errorMsg

HKS_TAG_PARAM4_UINT32 -> keyAliasHash
HKS_TAG_PARAM5_UINT32 -> keyHash
HKS_TAG_PARAM6_UINT32 -> renameDstKeyAliasHash
*/

int32_t AddKeyHash(struct HksParamSet *paramSetOut, const struct HksBlob *keyIn)
{
    uint16_t keyHash;
    int32_t ret = GetKeyHash(keyIn, &keyHash);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "GetKeyHash Failed!!")
    struct HksParam keyParams;
    keyParams.tag = HKS_TAG_PARAM5_UINT32;
    keyParams.uint32Param = (uint32_t)keyHash;
    ret = HksAddParams(paramSetOut, &keyParams, 1);
    if (ret != HKS_SUCCESS) {
        HKS_LOG_E("add keyHash to paramSet failed");
        HksFreeParamSet(&paramSetOut);
    }
    return ret;
}

int32_t AddKeyAliasHash(struct HksParamSet *paramSetOut, const struct HksBlob *keyAlias)
{
    uint8_t keyAliasHash;
    int32_t ret = GetKeyAliasHash(keyAlias, &keyAliasHash);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "GetKeyAliasHash Failed!!")
    struct HksParam hashKeyAliasParam = {
        .tag = HKS_TAG_PARAM4_UINT32,
        .uint32Param = (uint32_t)keyAliasHash
    };
    ret = HksAddParams(paramSetOut, &hashKeyAliasParam, 1);
    if (ret != HKS_SUCCESS) {
        HKS_LOG_E("add in params failed");
        HksFreeParamSet(&paramSetOut);
    }
    return ret;
}

static int32_t AddErrorMessage(struct HksParamSet *paramSetOut)
{
    const char *errMsg = HksGetErrorMsg();
    struct HksParam param = {
        .tag = HKS_TAG_PARAM0_NULL,
        .blob.size = strlen(errMsg), .blob.data = (uint8_t*)errMsg,
    };
    int32_t ret = HksAddParams(paramSetOut, &param, 1);
    if (ret != HKS_SUCCESS) {
        HKS_LOG_E("add time cost to params failed");
        HksFreeParamSet(&paramSetOut);
    }
    return ret;
}

int32_t AddTimeCost(struct HksParamSet *paramSetOut, uint64_t startTime)
{
    uint64_t endTime = 0;
    (void)HksElapsedRealTime(&endTime);
    uint32_t totalCost = (uint32_t)(startTime - endTime);
    struct HksParam param = {
        .tag = HKS_TAG_PARAM3_UINT32,
        .uint32Param = totalCost
    };
    int32_t ret = HksAddParams(paramSetOut, &param, 1);
    if (ret != HKS_SUCCESS) {
        HKS_LOG_E("add time cost to params failed");
        HksFreeParamSet(&paramSetOut);
    }
    return ret;
}

static int32_t AddProcessInfo(struct HksParamSet *paramSetOut, const struct HksProcessInfo *processInfo)
{
    struct HksParam params[] = {
        {
            .tag = HKS_TAG_PARAM2_BUFFER,
            .blob.size = processInfo->processName.size,
            .blob.data = (uint8_t*)processInfo->processName.data,
        },
        {
            .tag = HKS_TAG_PARAM2_UINT32,
            .uint32Param = processInfo->uidInt,
        }
    };
    int32_t ret = HksAddParams(paramSetOut, params, sizeof(params) / sizeof(params[0]));
    if (ret != HKS_SUCCESS) {
        HKS_LOG_E("add in params failed");
        HksFreeParamSet(&paramSetOut);
    }
    return ret;
}

static int32_t AddFuncName(struct HksParamSet *paramSetOut, const char *funcName)
{
    struct HksParam params  = {
        .tag = HKS_TAG_PARAM0_BUFFER,
        .blob.size = strlen(funcName),
        .blob.data = (uint8_t*)funcName
    };
    int32_t ret = HksAddParams(paramSetOut, &params, 1);
    if (ret != HKS_SUCCESS) {
        HKS_LOG_E("add in params failed");
        HksFreeParamSet(&paramSetOut);
    }
    return ret;
}

int32_t AddCommonInfo(const char *funcName, const struct HksProcessInfo *processInfo,
    int32_t errorCode, struct HksParamSet *reportParamSet)
{
    int32_t ret = AddProcessInfo(reportParamSet, processInfo);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "add processInfo to params failed")
    ret = AddFuncName(reportParamSet, funcName);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "add function name to params failed")
    ret = AddErrorMessage(reportParamSet);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "add error message to params failed")
    struct HksEventResultInfo resultInfo = {
        .code = errorCode,
        .module = 0,
        .stage = 0,
        .errMsg = HksGetErrorMsg()
    };
    struct timespec time;
    timespec_get(&time, TIME_UTC);
    struct HksParam params[] = {
        {
            .tag = HKS_TAG_PARAM1_BUFFER,
            .blob.size = sizeof(time), .blob.data = (uint8_t*)&time
        },
        {
            .tag = HKS_TAG_PARAM3_BUFFER,
            .blob.size = sizeof(resultInfo), .blob.data = (uint8_t*)&resultInfo
        }
    };
    ret = HksAddParams(reportParamSet, params, sizeof(params) / sizeof(params[0]));
    if (ret != HKS_SUCCESS) {
        HKS_LOG_E("add in params failed");
        HksFreeParamSet(&reportParamSet);
    }
    return ret;
}

int32_t GetCommonEventInfo(const struct HksParamSet *paramSetIn, struct HksEventInfo *eventInfo)
{
    struct HksParam *paramToEventInfo;
    int32_t ret = HksGetParam(paramSetIn, HKS_TAG_PARAM0_UINT32, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get eventId failed!");
    eventInfo->common.eventId = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_PARAM0_BUFFER, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get function name failed!");
    eventInfo->common.function = (char*)paramToEventInfo->blob.data;

    ret = HksGetParam(paramSetIn, HKS_TAG_PARAM1_UINT32, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get operation failed!");
    eventInfo->common.operation = paramToEventInfo->uint32Param;

    eventInfo->common.count = 1;

    ret = HksGetParam(paramSetIn, HKS_TAG_PARAM1_BUFFER, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get timespec failed!");
    eventInfo->common.time = *(struct timespec *)paramToEventInfo->blob.data;

    ret = HksGetParam(paramSetIn, HKS_TAG_PARAM2_BUFFER, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get process name failed!");
    eventInfo->common.callerInfo.name = (char*)paramToEventInfo->blob.data;

    ret = HksGetParam(paramSetIn, HKS_TAG_PARAM2_UINT32, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get process uid failed!");
    eventInfo->common.callerInfo.uid = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_PARAM3_BUFFER, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get HksEventResultInfo failed!");
    eventInfo->common.result = *(struct HksEventResultInfo*)paramToEventInfo->blob.data;

    return ret;
}

int32_t GetEventKeyInfo(const struct HksParamSet *paramSetIn, struct HksEventKeyInfo *keyInfo)
{
    struct HksParam *paramToEventInfo;
    int32_t ret = HksGetParam(paramSetIn, HKS_TAG_PARAM4_UINT32, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get key alias hash (1 byte) failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->aliasHash = paramToEventInfo->int32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_AUTH_STORAGE_LEVEL, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get storage level failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->storageLevel = *paramToEventInfo->blob.data;

    ret = HksGetParam(paramSetIn, HKS_TAG_SPECIFIC_USER_ID, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get specificUserId failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->specificUserId = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_ALGORITHM, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get algorithm failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->alg = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_PURPOSE, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get purpose failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->purpose = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_KEY_SIZE, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get keySize failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->keySize = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_KEY_FLAG, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get keyFlag failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->keyFlag = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_PARAM5_UINT32, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get key hash (2 bytes) failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->keyHash = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_IS_BATCH_OPERATION, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get isBatch failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->isBatch = paramToEventInfo->boolParam;

    ret = HksGetParam(paramSetIn, HKS_TAG_BATCH_PURPOSE, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get batchPurpose failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->batchPur = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_BATCH_OPERATION_TIMEOUT, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get batchTimeOut failed! ret = %" LOG_PUBLIC "d", ret);
    keyInfo->batchTimeOut = paramToEventInfo->uint32Param;
    return ret;
}

int32_t GetEventKeyAccessInfo(const struct HksParamSet *paramSetIn, struct HksEventKeyAccessInfo *keyAccessInfo)
{
    struct HksParam *paramToEventInfo;
    int32_t ret = HksGetParam(paramSetIn, HKS_TAG_USER_AUTH_TYPE, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get authType failed! ret = %" LOG_PUBLIC "d", ret);
    keyAccessInfo->authType = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_KEY_AUTH_ACCESS_TYPE, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get accessType failed! ret = %" LOG_PUBLIC "d", ret);
    keyAccessInfo->accessType = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_CHALLENGE_TYPE, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get challengeType failed! ret = %" LOG_PUBLIC "d", ret);
    keyAccessInfo->challengeType = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_CHALLENGE_POS, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get challengePos failed! ret = %" LOG_PUBLIC "d", ret);
    keyAccessInfo->challengePos = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_AUTH_TIMEOUT, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get authTimeOut failed! ret = %" LOG_PUBLIC "d", ret);
    keyAccessInfo->authTimeOut = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_KEY_AUTH_PURPOSE, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get authPurpose failed! ret = %" LOG_PUBLIC "d", ret);
    keyAccessInfo->authPurpose = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_FRONT_USER_ID, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get frontUserId failed! ret = %" LOG_PUBLIC "d", ret);
    keyAccessInfo->frontUserId = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_USER_AUTH_MODE, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get frontUserId failed! ret = %" LOG_PUBLIC "d", ret);
    keyAccessInfo->authMode = paramToEventInfo->uint32Param;

    ret = HksGetParam(paramSetIn, HKS_TAG_IS_DEVICE_PASSWORD_SET, &paramToEventInfo);
    HKS_IF_NOT_SUCC_LOGE(ret, "report get isNeedPwdSet failed! ret = %" LOG_PUBLIC "d", ret);
    keyAccessInfo->needPwdSet = paramToEventInfo->boolParam;
    return ret;
}

void EventInfoToMapKeyInfo(const struct HksEventInfo *eventInfo, struct HksEventItemMap *map, uint32_t *num)
{
    struct HksEventItemMap *curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "alias_hash");
    curMap->value = eventInfo->keyInfo.aliasHash;
    
    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "storage_level");
    curMap->value = eventInfo->keyInfo.storageLevel;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "specific_os_account_id");
    curMap->value = eventInfo->keyInfo.specificUserId;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "algorithm");
    curMap->value = eventInfo->keyInfo.alg;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "purpose");
    curMap->value = eventInfo->keyInfo.purpose;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "key_size");
    curMap->value = eventInfo->keyInfo.keySize;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "key_flag");
    curMap->value = eventInfo->keyInfo.keyFlag;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "key_hash");
    curMap->value = eventInfo->keyInfo.keyHash;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "batch_operation");
    curMap->value = eventInfo->keyInfo.isBatch;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "batch_purpose");
    curMap->value = eventInfo->keyInfo.batchPur;
    
    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "batch_timeout");
    curMap->value = eventInfo->keyInfo.batchTimeOut;
}

void EventInfoToMapKeyAccessInfo(const struct HksEventInfo *eventInfo, struct HksEventItemMap *map, uint32_t *num)
{
    struct HksEventItemMap *curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "auth_type");
    curMap->value = eventInfo->keyAccessInfo.authType;
    
    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "access_type");
    curMap->value = eventInfo->keyAccessInfo.accessType;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "challenge_type");
    curMap->value = eventInfo->keyAccessInfo.challengeType;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "challenge_pos");
    curMap->value = eventInfo->keyAccessInfo.challengePos;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "auth_timeout");
    curMap->value = eventInfo->keyAccessInfo.authTimeOut;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "auth_purpose");
    curMap->value = eventInfo->keyAccessInfo.authPurpose;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "front_os_account_id");
    curMap->value = eventInfo->keyAccessInfo.frontUserId;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "auth_mode");
    curMap->value = eventInfo->keyAccessInfo.authMode;

    curMap = map + (*num)++;
    (void)strcpy_s(curMap->name, sizeof(curMap->name), "need_pwd_set");
    curMap->value = eventInfo->keyAccessInfo.needPwdSet;
}