/*
 * Copyright (c) 2025-2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hks_report_common_build.h"

#include <stdint.h>
#include <string.h>

#include "hks_event_info.h"
#include "hks_log.h"
#include "hks_param.h"
#include "hks_template.h"
#include "hks_type.h"
#include "hks_type_inner.h"
#include "stdbool.h"
#include "time.h"

int32_t GetReportInfo(const struct HksParamSet *paramSet, HksOperationReport **report)
{
    struct HksParam *param = NULL;
    int32_t ret = HksGetParam(paramSet, HKS_TAG_PARAM1_NULL, &param);
    HKS_IF_NOT_SUCC_LOGE_RETURN(ret, ret, "get operation report fail")
    *report = (HksOperationReport *)param->blob.data;
    return HKS_SUCCESS;
}

void BuildCommonInfo(const struct HksParamSet *paramSet, struct HksEventCommonInfo *commonInfo,
    const HksOperationReport *report)
{
    commonInfo->eventId = report->eventId;
    commonInfo->operation = report->purpose;
    struct HksBlob funcBlob = { 0, (uint8_t *)commonInfo->function };
    struct HksBlob timeBlob = { sizeof(struct timespec), (uint8_t *)&commonInfo->time };
    struct HksBlob callerNameBlob = { 0, (uint8_t *)commonInfo->callerInfo.name };
    struct HksBlob resultBlob = { sizeof(HksEventResultInfo), (uint8_t *)&commonInfo->result };
    struct HksBlob errMsgBlob = { 0, (uint8_t *)commonInfo->result.errMsg };
    const HksTagValueMap commonInfoTagValues[] = {
        { HKS_TAG_PARAM0_BUFFER, &funcBlob, 0 },
        { HKS_TAG_PARAM1_BUFFER, &timeBlob, timeBlob.size },
        { HKS_TAG_PARAM2_UINT32, &commonInfo->callerInfo.uid, sizeof(commonInfo->callerInfo.uid) },
        { HKS_TAG_PARAM2_BUFFER, &callerNameBlob, 0 },
        { HKS_TAG_PARAM3_BUFFER, &resultBlob, resultBlob.size },
        { HKS_TAG_PARAM0_NULL, &errMsgBlob, 0 },
    };
    int32_t ret;
    for (uint32_t i = 0; i < HKS_ARRAY_SIZE(commonInfoTagValues); i++) {
        ret = GetParamValue(paramSet, commonInfoTagValues[i].tag, commonInfoTagValues[i].value,
            commonInfoTagValues[i].size);
        HKS_IF_NOT_SUCC_LOGE(ret, "add common info param[%d] fail", i)
    }

    commonInfo->count = 1;
}

int32_t BuildKeyInfo(const struct HksParamSet *paramSet, struct HksEventKeyInfo *keyInfo,
    const HksOperationReport *report)
{
    keyInfo->purpose = report->purpose;
    keyInfo->aliasHash = report->keyAliasHash;
    keyInfo->keyHash = report->keyHash;
    const HksTagValueMap keyInfoTagValues[] = {
        { HKS_TAG_AUTH_STORAGE_LEVEL, &keyInfo->storageLevel, sizeof(keyInfo->storageLevel) },
        { HKS_TAG_SPECIFIC_USER_ID, &keyInfo->specificUserId, sizeof(keyInfo->specificUserId) },
        { HKS_TAG_ALGORITHM, &keyInfo->alg, sizeof(keyInfo->alg) },
        { HKS_TAG_KEY_SIZE, &keyInfo->keySize, sizeof(keyInfo->keySize) },
        { HKS_TAG_KEY_FLAG, &keyInfo->keyFlag, sizeof(keyInfo->keyFlag) },
    };
    int32_t ret;
    for (uint32_t i = 0; i < HKS_ARRAY_SIZE(keyInfoTagValues); i++) {
        ret = GetParamValue(paramSet, keyInfoTagValues[i].tag, keyInfoTagValues[i].value, keyInfoTagValues[i].size);
        HKS_IF_NOT_SUCC_LOGE(ret, "add key info param[%d] fail", i)
    }

    return HKS_SUCCESS;
}

void BuildKeyAccessInfo(const struct HksParamSet *paramSet, struct HksEventKeyAccessInfo *accessInfo,
    const HksOperationReport *report)
{
    accessInfo->authType = report->authType;
    accessInfo->accessType = report->accessType;
    accessInfo->challengeType = report->challengeType;
    accessInfo->challengePos = report->challengePos;
    accessInfo->authTimeOut = report->authTimeOut;
    accessInfo->authPurpose = report->authPurpose;
    accessInfo->frontUserId = report->frontUserId;
    accessInfo->authMode = report->authMode;
    accessInfo->needPwdSet = report->needPwdSet;
}

void BuildCryptoInfo(const struct HksParamSet *paramSet, struct HksEventCryptoInfo *cryptoInfo,
    const HksOperationReport *report)
{
    cryptoInfo->dataLen = report->dataLen;
    cryptoInfo->stat.initCost = report->initCost;
    cryptoInfo->stat.updateCost = report->updateCost;
    cryptoInfo->stat.finishCost = report->finishCost;
    cryptoInfo->stat.totalCost = report->totalCost;
    const HksTagValueMap cryptoTagValues[] = {
        { HKS_TAG_BLOCK_MODE, &cryptoInfo->blockMode, sizeof(cryptoInfo->blockMode) },
        { HKS_TAG_PADDING, &cryptoInfo->padding, sizeof(cryptoInfo->padding) },
        { HKS_TAG_DIGEST, &cryptoInfo->digest, sizeof(cryptoInfo->digest) },
        { HKS_TAG_MGF_DIGEST, &cryptoInfo->mgfDigest, sizeof(cryptoInfo->mgfDigest) },
        { HKS_TAG_PARAM4_UINT32, &cryptoInfo->handleId, sizeof(cryptoInfo->handleId) },
    };
    int32_t ret;
    for (uint32_t i = 0; i < HKS_ARRAY_SIZE(cryptoTagValues); i++) {
        ret = GetParamValue(paramSet, cryptoTagValues[i].tag, cryptoTagValues[i].value, cryptoTagValues[i].size);
        HKS_IF_NOT_SUCC_LOGE(ret, "add crypto info param[%d] fail", i)
    }

}

void BuildAgreeDeriveInfo(const struct HksParamSet *paramSet, struct HksEventAgreeDeriveInfo *agreeDeriveInfo,
    const HksOperationReport *report)
{
    agreeDeriveInfo->stat.initCost = report->initCost;
    agreeDeriveInfo->stat.updateCost = report->updateCost;
    agreeDeriveInfo->stat.finishCost = report->finishCost;
    agreeDeriveInfo->stat.totalCost = report->totalCost;
    const HksTagValueMap agreeDeriveInfoTagValues[] = {
        { HKS_TAG_ITERATION, &agreeDeriveInfo->iterCnt, sizeof(agreeDeriveInfo->iterCnt) },
        { HKS_TAG_DERIVE_AGREE_KEY_STORAGE_FLAG, &agreeDeriveInfo->storageFlag, sizeof(agreeDeriveInfo->storageFlag) },
        { HKS_TAG_DERIVE_KEY_SIZE, &agreeDeriveInfo->keySize, sizeof(agreeDeriveInfo->keySize) },
        { HKS_TAG_AGREE_PUBKEY_TYPE, &agreeDeriveInfo->pubKeyType, sizeof(agreeDeriveInfo->pubKeyType) },
        { HKS_TAG_PARAM4_UINT32, &agreeDeriveInfo->handleId, sizeof(agreeDeriveInfo->handleId) },
        { HKS_TAG_PARAM3_UINT32, &agreeDeriveInfo->stat.totalCost, sizeof(agreeDeriveInfo->stat.totalCost) },
    };
    int32_t ret;
    for (uint32_t i = 0; i < HKS_ARRAY_SIZE(agreeDeriveInfoTagValues); i++) {
        ret = GetParamValue(paramSet, agreeDeriveInfoTagValues[i].tag, agreeDeriveInfoTagValues[i].value,
            agreeDeriveInfoTagValues[i].size);
        HKS_IF_NOT_SUCC_LOGE(ret, "add agree derive info param[%d] fail", i)
    }

}

void BuildMacInfo(const struct HksParamSet *paramSet, struct HksEventMacInfo *macInfo,
    const HksOperationReport *report)
{
    macInfo->dataLen = report->dataLen;
    macInfo->stat.initCost = report->initCost;
    macInfo->stat.updateCost = report->updateCost;
    macInfo->stat.finishCost = report->finishCost;
    macInfo->stat.totalCost = report->totalCost;
    const HksTagValueMap macInfoTagValues[] = {
        { HKS_TAG_PARAM4_UINT32, &macInfo->handleId },
        { HKS_TAG_PARAM3_UINT32, &macInfo->stat.totalCost },
    };
    int32_t ret;
    for (uint32_t i = 0; i < HKS_ARRAY_SIZE(macInfoTagValues); i++) {
        ret = GetParamValue(paramSet, macInfoTagValues[i].tag, macInfoTagValues[i].value, macInfoTagValues[i].size);
        HKS_IF_NOT_SUCC_LOGE(ret, "add mac info param[%d] fail", i)
    }

}

// check uid, eventId, operation, specificUserId, aliasHash, keyHash
bool CheckEventCommon(const struct HksEventInfo *info1, const struct HksEventInfo *info2)
{
    if ((info1 == NULL) || (info2 == NULL) ||
        (info1->common.callerInfo.uid != info2->common.callerInfo.uid) ||
        (info1->common.eventId != info2->common.eventId) ||
        (info1->common.operation != info2->common.operation)) {
        return false;
    }
    switch (info1->common.eventId) {
        case HKS_EVENT_CRYPTO:
            if (info1->cryptoInfo.keyInfo.specificUserId != info2->cryptoInfo.keyInfo.specificUserId ||
                info1->cryptoInfo.keyInfo.aliasHash != info2->cryptoInfo.keyInfo.aliasHash ||
                info1->cryptoInfo.keyInfo.keyHash != info2->cryptoInfo.keyInfo.keyHash) {
                return false;
            }
            break;
        case HKS_EVENT_AGREE_DERIVE:
            if (info1->agreeDeriveInfo.keyInfo.specificUserId != info2->agreeDeriveInfo.keyInfo.specificUserId ||
                info1->agreeDeriveInfo.keyInfo.aliasHash != info2->agreeDeriveInfo.keyInfo.aliasHash ||
                info1->agreeDeriveInfo.keyInfo.keyHash != info2->agreeDeriveInfo.keyInfo.keyHash) {
                return false;
            }
            break;
        case HKS_EVENT_MAC:
            if (info1->macInfo.keyInfo.specificUserId != info2->macInfo.keyInfo.specificUserId ||
                info1->macInfo.keyInfo.aliasHash != info2->macInfo.keyInfo.aliasHash ||
                info1->macInfo.keyInfo.keyHash != info2->macInfo.keyInfo.keyHash) {
                return false;
            }
            break;
        case HKS_EVENT_ATTEST:
            if (info1->attestInfo.keyInfo.specificUserId != info2->attestInfo.keyInfo.specificUserId ||
                info1->attestInfo.keyInfo.aliasHash != info2->attestInfo.keyInfo.aliasHash ||
                info1->attestInfo.keyInfo.keyHash != info2->attestInfo.keyInfo.keyHash) {
                return false;
            }
            break;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        default:
            HKS_LOG_E("purpose do not need report");
            return false;
    }
    return true;
}

// add count, dataLen, totalCost
void AddEventInfoCommon(HksEventInfo *info, const HksEventInfo *entry)
{
    info->common.count++;
    switch (info->common.eventId) {
        case HKS_EVENT_CRYPTO:
            info->cryptoInfo.dataLen += entry->cryptoInfo.dataLen;
            info->cryptoInfo.stat.taCost += entry->cryptoInfo.stat.totalCost;
            break;
        case HKS_EVENT_AGREE_DERIVE:
            info->agreeDeriveInfo.stat.taCost += entry->agreeDeriveInfo.stat.totalCost;
            break;
        case HKS_EVENT_MAC:
            info->macInfo.dataLen += entry->macInfo.dataLen;
            info->macInfo.stat.taCost += entry->macInfo.stat.totalCost;
            break;
        case HKS_EVENT_ATTEST:
            info->attestInfo.stat.taCost += entry->attestInfo.stat.totalCost;
            break;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        default:
            HKS_LOG_E("purpose do not need report");
    }
}

void KeyInfoToMap(const HksEventKeyInfo *keyInfo, HksEventItemMap *map, uint32_t *num)
{
    const HksEventItemMap keyInfoNameValues[] = {
        { "alias_hash", (uint32_t)keyInfo->aliasHash },
        { "storage_level", (uint32_t)keyInfo->storageLevel },
        { "specific_os_account_id", (uint32_t)keyInfo->specificUserId },
        { "algorithm", (uint32_t)keyInfo->alg },
        { "purpose", (uint32_t)keyInfo->purpose },
        { "key_size", (uint32_t)keyInfo->keySize },
        { "key_flag", (uint32_t)keyInfo->keyFlag },
        { "key_hash", (uint32_t)keyInfo->keyHash },
        { "batch_operation", (uint32_t)keyInfo->isBatch },
        { "batch_purpose", (uint32_t)keyInfo->batchPur },
        { "batch_timeout", (uint32_t)keyInfo->batchTimeOut },
    };

    for (uint32_t i = 0; i < HKS_ARRAY_SIZE(keyInfoNameValues); i++) {
        (void)strcpy_s(map[*num].name, sizeof(map[*num]), keyInfoNameValues[i].name);
        map[*num].value = keyInfoNameValues[i].value;
        (*num)++;
    }
}

void KeyAccessInfoToMap(const HksEventKeyAccessInfo *accessInfo, HksEventItemMap *map, uint32_t *num)
{
    const HksEventItemMap keyAccessInfoNameValues[] = {
        { "auth_type", (uint32_t)accessInfo->authType },
        { "access_type", (uint32_t)accessInfo->accessType },
        { "challenge_type", (uint32_t)accessInfo->challengeType },
        { "challenge_pos", (uint32_t)accessInfo->challengePos },
        { "auth_timeout", (uint32_t)accessInfo->authTimeOut },
        { "auth_purpose", (uint32_t)accessInfo->authPurpose },
        { "front_os_account_id", (uint32_t)accessInfo->frontUserId },
        { "auth_mode", (uint32_t)accessInfo->authMode },
        { "need_pwd_set", (uint32_t)accessInfo->needPwdSet },
    };

    for (uint32_t i = 0; i < HKS_ARRAY_SIZE(keyAccessInfoNameValues); i++) {
        (void)strcpy_s(map[*num].name, sizeof(map[*num]), keyAccessInfoNameValues[i].name);
        map[*num].value = keyAccessInfoNameValues[i].value;
        (*num)++;
    }
}

void CryptoInfoToMap(const HksEventCryptoInfo *cryptoInfo, HksEventItemMap *map, uint32_t *num)
{
    const HksEventItemMap cryptoInfoNameValues[] = {
        { "block_mode", (uint32_t)cryptoInfo->blockMode },
        { "padding", (uint32_t)cryptoInfo->padding },
        { "digest", (uint32_t)cryptoInfo->digest },
        { "mgf_digest", (uint32_t)cryptoInfo->mgfDigest },
        { "data_length", (uint32_t)cryptoInfo->dataLen },
        { "handle_id", (uint32_t)cryptoInfo->handleId },
        { "total_cost", (uint32_t)cryptoInfo->stat.totalCost },
    };

    for (uint32_t i = 0; i < HKS_ARRAY_SIZE(cryptoInfoNameValues); i++) {
        (void)strcpy_s(map[*num].name, sizeof(map[*num]), cryptoInfoNameValues[i].name);
        map[*num].value = cryptoInfoNameValues[i].value;
        (*num)++;
    }
}

void AgreeDeriveInfoToMap(const HksEventAgreeDeriveInfo *agreeDeriveInfo, HksEventItemMap *map, uint32_t *num)
{
    const HksEventItemMap agreeDeriveInfoNameValues[] = {
        { "iter_count", (uint32_t)agreeDeriveInfo->iterCnt },
        { "storage_falg", (uint32_t)agreeDeriveInfo->storageFlag },
        { "derive_key_size", (uint32_t)agreeDeriveInfo->keySize },
        { "agree_pubkey_type", (uint32_t)agreeDeriveInfo->pubKeyType },
        { "handle_id", (uint32_t)agreeDeriveInfo->handleId },
        { "total_cost", (uint32_t)agreeDeriveInfo->stat.totalCost },
    };

    for (uint32_t i = 0; i < HKS_ARRAY_SIZE(agreeDeriveInfoNameValues); i++) {
        (void)strcpy_s(map[*num].name, sizeof(map[*num]), agreeDeriveInfoNameValues[i].name);
        map[*num].value = agreeDeriveInfoNameValues[i].value;
        (*num)++;
    }
}

void MacInfoToMap(const HksEventMacInfo *macInfo, HksEventItemMap *map, uint32_t *num)
{
    const HksEventItemMap macInfoNameValues[] = {
        { "data_length", (uint32_t)macInfo->dataLen },
        { "handle_id", (uint32_t)macInfo->handleId },
        { "total_cost", (uint32_t)macInfo->stat.totalCost },
    };

    for (uint32_t i = 0; i < HKS_ARRAY_SIZE(macInfoNameValues); i++) {
        (void)strcpy_s(map[*num].name, sizeof(map[*num]), macInfoNameValues[i].name);
        map[*num].value = macInfoNameValues[i].value;
        (*num)++;
    }
}
